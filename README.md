# Setting up ssh and wifi networking before boot

(Note: this idea was sourced from a tutorial [here](https://www.raspberrypi.org/forums/viewtopic.php?t=191252))

- Mount your Raspbian distro SD card on a laptop or PC

- You will see a volume called 'boot'

- In the root of the 'boot' volume, add an empty file called 'ssh'.  In Linux, this can be done as 'touch ssh' on the command line.

- In the same directory (the root of the 'boot' volume), add a file called 'wpa_supplicant.conf', with the following contents:

```
country=US
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1

network={
ssid="your_real_wifi_ssid"
scan_ssid=1
psk="your_real_password"
key_mgmt=WPA-PSK
}
```

- When you boot up, after the Pi settles, if your PC / laptop is connected to the same Wifi, you should be able to ssh into your pi as pi@raspberrypi.local (or dex.local, if it's Raspbian for Robots)

- (Untested) This procedure for connecting to wifi may be repeatable -- every time you need to connect to a new wifi network, you can add a new file called 'wpa_supplicant.conf' in the 'boot' volume when the SD card is mounted on your PC or laptop, and this new configuration will again be copied and used by the RPi on boot. 

